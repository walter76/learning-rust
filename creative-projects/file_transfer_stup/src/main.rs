// Test it with the following commands:
// curl -X DELETE http://localhost:8080/datafile.txt
// curl -X GET http://localhost:8080/datafile.txt
// curl -X PUT http://localhost:8080/datafile.txt -d "File contents."
// curl -X POST http://localhost:8080/data -d "File contents."
// curl -X GET http://localhost:8080/a/b
//
// after running the second command, the client should have printed:
// Contents of the file.
//
// After running all five commands, the server should have printed:
// Listening at address 127.0.0.1:8080 ...
// Deleting file "datafile.txt" ... Deleted file "datafile.txt"
// Downloading file "datafile.txt" ... Downloaded file "datafile.txt"
// Uploading file "datafile.txt" ... Uploaded file "datafile.txt"
// Uploading file "data_*.txt" ... Uploaded file "data_17.txt"
// Invalid URI: "/a/b"

use actix_web::Error;
use futures::{
    future::{ok, Future},
    Stream,
};
use std::fs;
use std::io::{self, Write};

fn flush_stdout() {
    io::stdout().flush().unwrap();
}

fn delete_file(info: actix_web::web::Path<(String,)>) -> impl actix_web::Responder {
    let filename = &info.0;
    print!("Deleting file \"{}\" ... ", filename);
    flush_stdout();

    // Delete the file.
    match std::fs::remove_file(&filename) {
        Ok(_) => {
            println!("Deleted file \"{}\"", filename);
            actix_web::HttpResponse::Ok()
        }
        Err(error) => {
            println!("Failed to delete file \"{}\": {}", filename, error);
            actix_web::HttpResponse::NotFound()
        }
    }
}

fn download_file(info: actix_web::web::Path<(String,)>) -> impl actix_web::Responder {
    let filename = &info.0;
    print!("Downloading file \"{}\" ... ", filename);
    flush_stdout();

    // TODO Read the contents of the file.
    let contents = "Contents of the file.\n".to_string();

    println!("Downloaded file \"{}\"", filename);
    actix_web::HttpResponse::Ok()
        .content_type("text/plain")
        .body(contents)
}

fn upload_specified_file(
    payload: actix_web::web::Payload,
    info: actix_web::web::Path<(String,)>,
) -> impl Future<Item = actix_web::HttpResponse, Error = Error> {
    let filename = info.0.clone();

    print!("Uploading file \"{}\" ... ", filename);
    flush_stdout();

    // Get asynchronously from the client the contents to write into the file.
    payload
        .map_err(Error::from)
        .fold(actix_web::web::BytesMut::new(), move |mut body, chunk| {
            body.extend_from_slice(&chunk);
            Ok::<_, Error>(body)
        })
        .and_then(move |contents| {
            // Create the file.
            let f = fs::File::create(&filename);
            if f.is_err() {
                println!("Failed to create file \"{}\"", filename);
                return ok(actix_web::HttpResponse::NotFound().into());
            }

            // Write the contents into it.
            if f.unwrap().write_all(&contents).is_err() {
                println!("Failed to write file \"{}\"", filename);
                return ok(actix_web::HttpResponse::NotFound().into());
            }

            println!("Uploaded file \"{}\"", filename);
            ok(actix_web::HttpResponse::Ok().finish())
        })
}

fn upload_new_file(info: actix_web::web::Path<(String,)>) -> impl actix_web::Responder {
    let filename = &info.0;
    print!("Uploading file \"{}*.txt\" ... ", filename);
    flush_stdout();

    // TODO Get from the client the contents to write into the file.
    let _contents = "Contents of the file.\n".to_string();

    // TODO Generate new filename and create that file.
    let file_id = 17;

    let filename = format!("{}{}.txt", filename, file_id);

    // TODO Write the contents into the file.

    println!("Uploaded file \"{}\"", filename);
    actix_web::HttpResponse::Ok()
        .content_type("text/plain")
        .body(filename)
}

fn invalid_resource(req: actix_web::HttpRequest) -> impl actix_web::Responder {
    println!("Invalid URI: \"{}\"", req.uri());
    actix_web::HttpResponse::NotFound()
}

fn main() -> io::Result<()> {
    const SERVER_ADDRESS: &str = "127.0.0.1:8080";

    println!("Listening at address {} ...", SERVER_ADDRESS);

    actix_web::HttpServer::new(|| {
        actix_web::App::new()
            .service(
                actix_web::web::resource("/{filename}")
                    .route(actix_web::web::delete().to(delete_file))
                    .route(actix_web::web::get().to(download_file))
                    .route(actix_web::web::put().to_async(upload_specified_file))
                    .route(actix_web::web::post().to(upload_new_file)),
            )
            .default_service(actix_web::web::route().to(invalid_resource))
    })
    .bind(SERVER_ADDRESS)?
    .run()
}
