use std::str;

fn main() {
    // -----
    // Strings
    // -----

    // String conversions and impact on memory
    let _str = "Hi I'm a &str type"; // immutable, global lifetime
                                         // how is it really stored in memory?
                                         // or is it baked into the binary?
    let _string = String::from("Hi, I'm a String type"); // usual lifetime
                                                               // allocated on the heap
    
    // move and borrow with &str and String
    // functions example
    // https://hermanradtke.com/2015/05/03/string-vs-str-in-rust-functions.html
    // https://www.ameyalokare.com/rust/2017/10/12/rust-str-vs-String.html
    // https://stackoverflow.com/questions/24158114/what-are-the-differences-between-rusts-string-and-str/24159933#24159933
    // https://doc.rust-lang.org/std/string/struct.String.html#deref
    // https://doc.rust-lang.org/std/primitive.str.html


    // conversions
    let _s = "A string".to_string();

    // implicit conversions

    // create empty string
    {
        let s = String::new();
    }

    // (all the type annotations are superfluous)
    // A reference to a string allocated in read only memory
    // let pangram = "the quick brown fox jumps over the lazy dog";
    // is the same as below:
    let pangram: &'static str = "the quick brown fox jumps over the lazy dog";
    println!("Pangram: {}", pangram);

    // Iterate over words in reverse, no new string is allocated
    println!("Words in reverse");
    for word in pangram.split_whitespace().rev() {
        println!("> {}", word);
    }

    // Copy chars into a vector, sort and remove duplicates
    let mut chars: Vec<char> = pangram.chars().collect();
    chars.sort();
    chars.dedup();

    println!("{:?}", chars);

    // Create an empty and growable 'String'
    let mut string = String::new();
    for c in chars {
        // Insert a char at the end of string
        string.push(c);
        // Insert a string at the end of string
        string.push_str(", ");
    }

    println!("{}", string);

    // The trimmed string is a slice to the original string, hence no new
    // allocation is performed
    let chars_to_trim: &[char] = &[' ', ','];
    let trimmed_str: &str = string.trim_matches(chars_to_trim);
    println!("Used characters: {}", trimmed_str);

    // Heap allocate a string
    let alice = String::from("I like dogs");
    // Allocate new memory and store the modified string there
    let bob: String = alice.replace("dog", "cat");

    println!("Alice says: {}", alice);
    println!("Bob says: {}", bob);

    // -----
    // Literals and escapes
    // -----

    // You can use escapes to write bytes by their hexadecimal values...
    let byte_escape = "I'm writing \x52\x75\x73\x74!";
    println!("What are you doing\x3F (\\x3F means ?) {}", byte_escape);

    // ...or Unicode code points.
    let unicode_codepoint = "\u{211D}";
    let character_name = "\"DOUBLE-STRUCK CAPITAL R\"";

    println!("Unicode character {} (U+211D) is called {}", unicode_codepoint, character_name);

    let long_string = "String literals
                             can span multiple lines.
                             The linebreak and indentation here ->\
                             <- can be escaped too!";
    println!("{}", long_string);

    // -----
    // Raw string literals
    // -----

    let raw_str = r"Escapes don't work here: \x3F \u{211D}";
    println!("{}", raw_str);

    // If you need quotes in a raw string, add a pair of #s
    let quotes = r#"And then I said: "There is no escape!""#;
    println!("{}", quotes);

    // If you need "# in your string, just use more #s in the delimiter.
    // There is no limit for the number of #s you can use.
    let longer_delimiter = r###"A string with "# in it. And even "##!"###;
    println!("{}", longer_delimiter);

    // -----
    // Byte strings
    // -----

    // strings have to be UTF-8, non UTF-8 strings are handled as byte strings

    // Note that this is not actually a `&str`
    let bytestring: &[u8; 21] = b"this is a byte string";

    // Byte arrays don't have the `Display` trait, so printing them is a bit limited
    println!("A byte string: {:?}", bytestring);

    // Byte strings can have byte escapes...
    let escaped = b"\x52\x75\x73\x74 as bytes";
    // ...but no unicode escapes
    // let escaped = b"\u{211D} is not allowed";
    println!("Some escaped bytes: {:?}", escaped);

    // Raw byte strings work just like raw strings
    let raw_bytestring = br"\u{211D} is not escaped here";
    println!("{:?}", raw_bytestring);

    // Converting a byte array to `str` can fail
    if let Ok(my_str) = str::from_utf8(raw_bytestring) {
        println!("And the same text: '{}'", my_str);
    }

    let _quotes = br#"You can also use "fancier" formatting, \
                             like with normal raw strings"#;
    
    // Byte strings don't have to be UTF-8
    let shift_jis = b"\x82\xe6\x82\xa8\x82\xb1\x82\xbb"; // in SHIFT-JIS

    // But then they can't always be converted to `str`
    match str::from_utf8(shift_jis) {
        Ok(my_str) => println!("Conversion successful: '{}'", my_str),
        Err(e) => println!("Conversion failed: {:?}", e),
    };

    // -----
    // String manipulations
    // -----

    {
        let mut str = String::from("Hola");
        str.push(' ');
        str.push_str("Mundo");
    }

    {
        // `+` operator only works with `&str`
        let mut str = "Hola".to_string();

        // this works
        str = str + " mundo";

        // this will not work
        // str = str + " mundo".to_string();
        // this too
        // let mut s = "Hola" + " mundo";
    }

    // length of string
    {
        let str = "abcdefghijklmnopqrstuvwxyz";
        let size = str.len();
    }

    // check for empty string
    {
        let mut s = String::new();
        s.is_empty(); // true

        s.push('t');
        s.is_empty(); // false
    }

    // create a vector of chars from iterator
    {
        let str = "abcdefghijklmnopqrstuvwxyz";
        let chars:Vec<char> = str.chars().collect();
        println!("{}", chars.len());
    }

    // create a string from chars
    {
        let chars = ['R', 'u', 's', 't'];
        let s: String = chars.iter().collect();
        println!("{}", s);
    }

    // split strings
    {
        let str = "A beginning is a delicate time";
        let v: Vec<&str> = str.split(" ").collect();

        println!("{:?}", v);
    }

    // trim whitespace
    {
        let str = "  Hola mundo  ";
        str.trim();
    }

    // string prefix/suffix
    {
        let str = "Without change something sleeps inside us";
        str.starts_with("Without"); // true
        str.ends_with("us"); // true
    }

    {
        let mut str = "<b>Bold text</b>";
        str = str.strip_prefix("<b>").unwrap();
        str = str.strip_suffix("</b>").unwrap();
        println!("{}", str);
    }

    // string replace
    {
        let s1 = "A wall against the wind.";
        let s2 = s1.replace("wall", "feather").replace("against", "in");
        println!("{}", s2);
    }

    // -----
    // Regular expressions
    // -----

    // https://lise-henry.github.io/articles/optimising_strings.html
}
