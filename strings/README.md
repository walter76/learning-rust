# All You Ever Wanted To Know About: Rust Strings

## Rust String Types

The `String` type is the most common string type that has ownership over the
contents of the string. It has a close relationship with its borrowed
counterpart, the primitive `str`.

### UTF-8

`String`s are always valid UTF-8. This has a few implications, the first of
which is that if you need a non-UTF-8 string, consider `OsString`. It is
similar, but without the UTF-8 constraint. The second implication is that you
cannot index into a `String`:

```rs
let s = "hello";

println!("The first letter of s is {}", s[0]); // ERROR!!!
```

Indexing is intended to be a constant-time operation, but UTF-8 encoding does
not allow us to do this. Furthermore, it's not clear what sort of thing the
index should return: a byte, a codepoint, or a grapheme cluster. The `bytes` and
`chars` methods return iterators over the first two, respectively.

### Deref
 
 `String`s implement `Deref<Target=str>`, and so inehrit all of `str`'s methods.
 In addition, this means that you can pass a `String` to a function which takes
 a `&str` by using an ampersand (`&`):

```rs
fn takes_str(s: &str) { }

let s = String::from("Hello");

takes_str(&s);
```

This will create a `&str` from the `String` and pass it in. This conversion is
very inexpensive, and so generally, functions will accept `&str`s as arguments
unless they need a `String` fro some specific reason.

In certain cases Rust doesn't have enough information to make this conversion,
known as `Deref` coercion. In the following example a string slice `&'a str`
implements the trait `TraitExample`, and the function `example_func` takes
anything that implements the trait. In this case Rust would need to make two
implicit conversions, which Rust doesn't have the means to do. For that reason,
the following example will not compile.

```rs
trait TraitExample {}

impl<'a> TraitExample for &'a str {}

fn example_func<A: TraitExample>(example_arg: A) {}

let example_string = String::from("example_string");
example_func(&example_string);
```

There are two options that would work instead. The first would be to change line
`example_func(&example_string);` to `example_func(example_string.as_str());`,
using the method `as_str()` to explicitly extract the string slice containing
the string. The second way changes `example_func(&example_string);` to
`example_func(&*example_string);`. In this case we are dereferencing `String` to
a `str`, then referencing the `str` back to `&str`. The second way is more
idiomatic, however both work to do the conversion explicitly rather than relying
on the implicit conversion.

### Representation

A `String` is made up of three components: a pointer to some bytes, a lenght,
and a capacity. The pointer points to an internal buffer `String` uses to store
its data. The length is the number of bytes currently stored in the buffer, and
the capacity is the size of the buffer in bytes. As such, the length will always
be less than or equal to the capacity.

This buffer is always stored on the heap.

You can look at these with the `as_ptr`, `len`, and `capacity` methods:

```rs
use std::mem;

let story = String::from("Once upon a time...");

// Prevent automatically dropiing the String's data
let mut story = mem::ManuallyDrop::new(story);

let ptr = story.as_mut_ptr();
let len = story.len();
let capacity = story.capacity();

// story has nineteen bytes
assert_eq!(19, len);

// We can re-build a String out of ptr, len, and capacity. This is all unsafe
// because we are responsible for making sure the components are valid:
let s = unsafe { String::from_raw_parts(ptr, len, capacity) };

assert_eq!(String::from("Once upon a time..."), s);
```

If a `String` has enough capacity, adding elements to it will not re-allocate.
For example, consider this program:

```rs
let mut s = String::new();

println!("{}", s.capacity());

for _ in 0..5 {
    s.push_str("hello");
    println!("{}", s.capacity());
}
```

This will output the following:

```
0
5
10
20
20
40
```

At first, we have no memory allocated at all, but as we append to the string, it
increases its capacity appropriately. If we instead use the `with_capacity`
method to allocate the correct capacity initially:

```rs
let mut s = String::with_capacity(25);

println!("{}", s.capacity());

for _ in 0..5 {
    s.push_str("hello");
    println!("{}", s.capacity());
}
```

We end up with a different output:

```
25
25
25
25
25
25
```

Here, there's no need to allocate memory inside the loop.

### When to use

`String` is the dynamic heap string type, like `Vec`: use it when you need to
own or modify your string data.

`str`is an immutable[^1] sequence of UTF-8 bytes of dynamic length somewhere in
memory. Since the size is unknown, one can only handle it behind a pointer. This
means that `str` most commonly[^2] appears as `&str`: a reference to some UTF-8
data, normally called a "string slice" or just a "slice". A slice is just a view
onto some data, and that data can be anywhere, e.g.

* **In static storage:** a string literal `"foo"` is a `&'static str`. The data
  is hardcoded into the executable and loaded into memory when the program runs.
* **Inside a heap allocated `String`:** `String` dereferences to a `&str` view
  of the `String`'s data.
* **On the stack:** e.g. the following creates a stack-allocated byte-array, and
  then gets a view of that data as a `&str`:
  ```rs
  use std::str;

  let x: &[u8] = &[b'a', b'b', b'c'];
  let stack_str: &str = str::from_utf8(x).unwrap();
  ```

In summary, use `String` if you need owned string data (like passing strings to
other threads, or building them at runtime), and use `&str` if you only need a
view of a string.

This is identical to the relationship between a vector `Vec<T>` and a slice
`&[T]`, and is similar to the relationship between by-value `T` and by-reference
`&T` for general types.

---

[^1]: A `str` is fixed-length; you cannot write bytes beyond the end, or leave
      trailing invalid bytes. Since UTF-8 is a variable-width encoding, this
      effectively forces all `str`s to be immutable in many cases. In general,
      mutation requires writing more or fewer bytes than there were before (e.g.
      replacing an `a` (1 byte) with an `ä` (2+ bytes) would require making more
      room in the `str`). There are specific methods that can modify a
      `&mut str` in place, mostly those that handle only ASCII characters, like
      `make_ascii_uppercase`.

[^2]: Dynamically sized types allow things like `Rc<str>` for a sequence of
      reference counted UTF-8 bytes since Rust 1.2. Rust 1.21 allows easily
      creating these types.