
use criterion::{criterion_group, criterion_main, Criterion};

// https://lise-henry.github.io/articles/optimising_strings.html

fn naive(input: &str) -> String {
    let mut output = String::new();
    for c in input.chars() {
        match c {
            '<' => output.push_str("&lt;"),
            '>' => output.push_str("&gt;"),
            '&' => output.push_str("&amp;"),
            _ => output.push(c)
        }
    }
    output
}

fn naive_capacity(input: &str) -> String {
    let mut output = String::with_capacity(input.len());
    for c in input.chars() {
        match c {
            '<' => output.push_str("&lt;"),
            '>' => output.push_str("&gt;"),
            '&' => output.push_str("&amp;"),
            _ => output.push(c)
        }
    }
    output
}

const NO_HTML_SHORT: &'static str = "A paragraph without HTML characters that need to be escaped.";
const NO_HTML_LONG: &'static str = "Another paragraph without characters that need to be escaped. This paragraph is a bit longer, as sometimes there can be large paragraphs that don't any special characters, e.g., in novels or whatever.";
const HTML_SHORT: &'static str = "Here->An <<example>> of rust codefn foo(u: &u32) -> &u32 {u}";
const HTML_LONG: &'static str = "A somewhat longer paragraph containing a character that needs to be escaped, because e.g. the author mentions the movie Fast&Furious in it. This paragraph is also quite long to match the non-html one.";

fn bench_naive_no_html(c: &mut Criterion) {
    c.bench_function("naive_no_html", |b| {
        b.iter(|| {
            naive(NO_HTML_SHORT);
            naive(NO_HTML_LONG);
        })
    });
}

fn bench_naive_html(c: &mut Criterion) {
    c.bench_function("bench_naive_html", |b| {
        b.iter(|| {
            naive(HTML_SHORT);
            naive(HTML_LONG);
        })
    });
}

fn bench_naive_all(c: &mut Criterion) {
    c.bench_function("bench_naive_all", |b| {
        b.iter(|| {
            naive(NO_HTML_SHORT);
            naive(NO_HTML_LONG);
            naive(HTML_SHORT);
            naive(HTML_LONG);
        })
    });
}

fn bench_naive_capacity_no_html(c: &mut Criterion) {
    c.bench_function("naive_capacity_no_html", |b| {
        b.iter(|| {
            naive_capacity(NO_HTML_SHORT);
            naive_capacity(NO_HTML_LONG);
        })
    });
}

fn bench_naive_capacity_html(c: &mut Criterion) {
    c.bench_function("naive_capacity_html", |b| {
        b.iter(|| {
            naive_capacity(HTML_SHORT);
            naive_capacity(HTML_LONG);
        })
    });
}

fn bench_naive_capacity_all(c: &mut Criterion) {
    c.bench_function("naive_capacity_all", |b| {
        b.iter(|| {
            naive_capacity(NO_HTML_SHORT);
            naive_capacity(NO_HTML_LONG);
            naive_capacity(HTML_SHORT);
            naive_capacity(HTML_LONG);
        })
    });
}

criterion_group!(benches,
    bench_naive_no_html, bench_naive_html, bench_naive_all,
    bench_naive_capacity_no_html, bench_naive_capacity_html, bench_naive_capacity_all
);

criterion_main!(benches);
