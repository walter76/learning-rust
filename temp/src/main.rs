use chrono::Utc;
use crossterm::event::{poll, read, Event, KeyCode};
use std::{io::Read, time::Duration};

use serial::SerialPort;

const SERIAL_PORT: &str = "COM4";
const SERIAL_BAUDRATE: serial::BaudRate = serial::Baud9600;
const SERIAL_TIMEOUT: Duration = Duration::from_secs(60);

const SERIAL_PORT_SETTINGS: serial::PortSettings = serial::PortSettings {
    baud_rate: SERIAL_BAUDRATE,
    char_size: serial::Bits8,
    parity: serial::ParityNone,
    stop_bits: serial::Stop1,
    flow_control: serial::FlowNone,
};

fn main() {
    println!(
        "Reading temperature and humidity from sensor connected to {} ({:?} baud rate).",
        SERIAL_PORT, SERIAL_BAUDRATE
    );
    println!("Press <Esc> to abort");
    println!();

    let mut serial_port = serial::open(SERIAL_PORT).unwrap();
    serial_port.configure(&SERIAL_PORT_SETTINGS).unwrap();
    serial_port.set_timeout(SERIAL_TIMEOUT).unwrap();

    let mut chunk: [u8; 8] = [0; 8];
    let mut stack_of_chars: Vec<char> = vec![];
    loop {
        if poll(Duration::from_millis(1_000)).unwrap() {
            let event = read().unwrap();

            if event == Event::Key(KeyCode::Esc.into()) {
                break;
            }
        }

        let bytes_read = serial_port.read(&mut chunk).unwrap();

        for index in 0..bytes_read {
            let c = chunk[index] as char;
            match c {
                '\n' => {
                    let line: String = stack_of_chars.iter().collect();
                    println!("[{}] {}", Utc::now(), line);

                    stack_of_chars.clear();
                }
                _ => stack_of_chars.push(c),
            }
        }
    }
}
