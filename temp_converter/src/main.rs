use std::io;

fn main() {
    println!("Temperature converter - Fahrenheit to Celcius");

    println!("Enter a temperature in Fahrenheit:");

    let mut fahrenheit = String::new();
    io::stdin()
        .read_line(&mut fahrenheit)
        .expect("Please enter a valid temperature in Fahrenheit");

    let fahrenheit: f64 = fahrenheit
        .trim()
        .parse()
        .expect("Please enter a valid temperature in Fahrenheit");

    println!("You entered: {}", fahrenheit);

    let celcius = (fahrenheit - 32.0) * (5.0 / 9.0);

    println!("{} Fahrenheit are {} °C in Celcius", fahrenheit, celcius);
}
