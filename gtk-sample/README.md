# Using gtk-rs to create a GUI application with Rust on Windows

## Prerequisites

You need to have Gtk+ installed. You can get it from the [website](https://www.gtk.org/download/windows.php).
For Gtk+ you will also need [MSYS2](https://msys2.github.io/) installed, but you will need this anyways. It is also
explained on Gtk+ how to install it as a prerequisite.

## Preparing gtk-rs

gtk-rs will only work with the GNU ABI toolchain. You need to make this work on Windows. See
[gtk-rs requirements](http://gtk-rs.org/docs-src/requirements.html) for more details.

Make sure you have the [GNU ABI](https://github.com/rust-lang-nursery/rustup.rs/blob/master/README.md#working-with-rust-on-windows)
version of the rust compiler installed. Contrary to earlier instructions, **you don't need to uncheck "Linker and
platform libraries" in the Rust setup or delete `gcc.exe` and `ld.exe` in Rust's `bin` directory.**

If you happen to having the Visual C++ toolchain installed (as I did), you can switch the active toolchain with

```
rustup default stable-x86_64-pc-windows-gnu
```

You can have multiple toolchains installed in parallel. If you are not sure, which toolchain you are currently using,
you can show your configuration with

```
rustup show
```
