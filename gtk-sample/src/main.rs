use gtk::prelude::*;
use gtk::{Application, ApplicationWindow};

fn main() {
    let app = Application::builder()
        .application_id("org.example.HelloWorld")
        .build();

    app.connect_activate(|app| {
        // we create the main window
        let win = ApplicationWindow::builder()
            .application(app)
            .default_width(320)
            .default_height(200)
            .title("Hello, World!")
            .build();

        // don't forget to make all widgets visible
        win.show_all();
    });

    app.run();

    // if gtk::init().is_err() {
    //     println!("Failed to initialize GTK.");
    //     return;
    // }

    // let window = Window::new(WindowType::Toplevel);
    // window.set_title("First GTK+ Program");
    // window.set_default_size(350, 70);
    // let button = Button::new_with_label("Click me!");
    // window.add(&button);
    // window.show_all();

    // window.connect_delete_event(|_, _| {
    //     gtk::main_quit();
    //     Inhibit(false)
    // });

    // button.connect_clicked(|_| {
    //     println!("Clicked");
    // });

    // gtk::main();
}
