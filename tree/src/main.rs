use std::cell::RefCell;
use std::rc::Rc;

// TODO a parent is not needed:
// "In general a node in a tree will not have pointers to its parents, but this
// information can be included (expanding the data structure to also include a
// pointer to the parent) or stored separately. Alternatively, upward links can
// be included in the child node data, as in a threaded binary tree."
// (https://en.wikipedia.org/wiki/Tree_(data_structure))
// 
// In Rust upward link is hard to achieve due to ownership rules. But, I do not
// really need it.
//
// TODO implement the visitor next

struct TreeNode<T> {
    value: T,
    children: Option<Vec<Rc<RefCell<TreeNode<T>>>>>,
    parent: Option<Rc<RefCell<TreeNode<T>>>>,
}

impl<T> TreeNode<T> {
    fn new(value: T) -> Self {
        Self {
            value,
            children: None,
            parent: None,
        }
    }

    fn add_child(&mut self, child: &Rc<RefCell<TreeNode<T>>>) {
        if self.children.is_none() {
            self.children = Some(Vec::new());
        }

        match &mut self.children {
            Some(children) => children.push(Rc::clone(child)),
            None => (),
        }
    }

    fn add_parent(&mut self, parent: &Rc<RefCell<TreeNode<T>>>) {
        self.parent = Some(Rc::clone(parent));
    }
}

#[cfg(test)]
mod tree_node_tests {
    use std::cell::RefCell;
    use std::rc::Rc;

    use super::TreeNode;

    #[test]
    fn create_root_node() {
        const ROOT_NODE_VALUE: i32 = 1;

        let root_node = TreeNode::new(ROOT_NODE_VALUE);

        assert!(root_node.children.is_none());
        assert!(root_node.parent.is_none());
        assert_eq!(ROOT_NODE_VALUE, root_node.value);
    }

    #[test]
    fn create_root_node_with_child() {
        const ROOT_NODE_VALUE: i32 = 1;
        const CHILD_NODE_VALUE: i32 = 11;

        let root_node = Rc::new(RefCell::new(TreeNode::new(ROOT_NODE_VALUE)));
        let child_node = Rc::new(RefCell::new(TreeNode::new(CHILD_NODE_VALUE)));
        root_node.borrow_mut().add_child(&child_node);

        child_node.borrow_mut().add_parent(&root_node);

        assert!(root_node.borrow().children.is_some());
        assert!(root_node.borrow().parent.is_none());
        match root_node.borrow().children.as_ref() {
            Some(children) => assert_eq!(children.len(), 1),
            None => assert!(false),
        };

        assert!(child_node.borrow().parent.is_some());
    }
}

fn main() {
    println!("Hello, world!");
}
