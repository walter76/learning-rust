/// Representation of our possible errors.
/// Each variant will contain a string for more
/// detailed description.
#[derive(Debug)]
pub enum Error {
    /// An error related to the first 16 bytes in a sqlite3 file
    HeaderString(String),
    /// An error parsing the PageSize of a sqlite3 file
    InvalidPageSize(String),
    /// An error parsing the maximum/minimum payload fraction
    /// or leaf fraction
    InvalidFraction(String),
    /// The change counter failed to parse
    InvalidChangeCounter(String),
}

// impl Trait for Struct is a way to comply with
// a trait's definition
impl std::fmt::Display for Error {
    /// This will be called whenever we use display ({}) print
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        // the write! macro here works a lot like `format!` though it takes a first argument
        // that can be written to, which the argument `f` fulfils for us
        match self {
            Self::HeaderString(v) => write!(f,
                "Unexpected bytes at start of file, \
                expected the magic string 'SQLite format 3\u{0}', \
                found {:?}", v),
            Self::InvalidPageSize(msg) => write!(f,
                "Invalid page size {}", msg),
            Self::InvalidFraction(msg) => write!(f, "{}", msg),
            Self::InvalidChangeCounter(msg) => write!(f, "Invalid change counter: {}", msg),
        }
    }
}

// Error doesn't have any required methods
// so we can use an empty `impl` block to satisfy
// this trait's implementation
impl std::error::Error for Error {}
