// we import that Error enum we just defined
use crate::error::Error;
// we are still going to use `TryInto` for converting
// a slice to an array, we just move it from main.rs into here
// however we are going to leverage `TryFrom` to convert a `u16`
// into a `PageSize` so we bring that into scope here as well.
use std::convert::{TryFrom, TryInto};

pub fn parse_header(contents: &[u8])
    -> Result<(PageSize, FormatVersion, FormatVersion, u8, u32), Error>
{
    // Check that the first 16 bytes match the header string
    validate_header_string(&contents)?;
    // capture the page size
    let page_size = parse_page_size(&contents)?;
    // capture the write format version
    let write_version = FormatVersion::from(contents[18]);
    // capture the read format version
    let read_version = FormatVersion::from(contents[19]);
    let reserved_bytes = contents[20];
    validate_fraction(contents[21], 64, "Maximum payload fraction")?;
    validate_fraction(contents[22], 32, "Minimum payload fraction")?;
    validate_fraction(contents[23], 32, "Leaf fraction")?;

    // we'll use our helper from the crate to try and parse
    // these 4 bytes as the change counter. If that fails, we
    // put the message inside of our `Error` using the `map_err`
    // method on `Result`. This allows us to again use the
    // ? to short circuit if it fails
    let change_counter = crate::try_parse_u32(&contents[24..28])
        .map_err(|msg| Error::InvalidChangeCounter(msg))?;

    Ok((page_size, write_version, read_version, reserved_bytes, change_counter))
}

/// The magic string at the start of all Sqlite3 database files is
/// `Sqlite format 3\{0}`, we keep this as a static slice of bytes since it
/// shouldn't ever change and the file we are reading is already bytes so
/// converting it to a string is unnecessary
static HEADER_STRING: &[u8] = &[
    //  S   q   l    i    t    e  ` `   f    o    r    m   a    t  ` `  3  \u{0}
    83, 81, 76, 105, 116, 101, 32, 102, 111, 114, 109, 97, 116, 32, 51, 0,
];

/// This struct will wrap our 32bit number allowing us
/// to control how it gets used later. This #[derive]
/// at the top just allows us to print this value
/// to the terminal using debug ({:?}) print
#[derive(Debug)]
pub struct PageSize(u32);

/// Validate that the bytes provided match the special string
/// at the start of Sqlite3 files
pub fn validate_header_string(bytes: &[u8]) -> Result<(), Error> {
    let buf = &bytes[0..16];

    // if the provided bytes don't match the static HEADER_STRING,
    // we return early
    if buf != HEADER_STRING {
        // since we only head this way on the error case, we convert the provided
        // value into a string. We don't want to error in our error path if it isn't valid utf8
        // so we again use `from_utf8_lossy` and then convert that into a string.
        return Err(Error::HeaderString(
            String::from_utf8_lossy(buf).to_string(),
        ));
    }

    Ok(())
}

/// Parse the page size bytes the header into a `PageSize`
pub fn parse_page_size(bytes: &[u8]) -> Result<PageSize, Error> {
    // first we try and covert the slice into an array. This returns a `Result`
    // so we can use the `map_err` method on that to convert a possible error here
    // into _our_ error. Doing it this way allows us to use the `?` operator at the
    // end which will return early if this fails.
    let page_size_bytes: [u8; 2] = bytes[16..18].try_into().map_err(|_| {
        Error::InvalidPageSize(format!("expected a 2 byte slice, found: {:?}", bytes))
    })?;
    // Now we can convert the value into a `u16`
    let raw_page_size = u16::from_be_bytes(page_size_bytes);
    // lastly we are going to use `try_into` method defined below
    // to finish the job
    raw_page_size.try_into()
}

// Another trait implementation, similar to `Display`
// This one though, takes a generic argument that says
// what the input should be.
impl TryFrom<u16> for PageSize {
    // We also have to add an "associated type" here that will
    // define the error we will return from the one method we
    // have to define
    type Error = Error;

    // This is the single requirement for conforming to `TryFrom`
    fn try_from(v: u16) -> Result<PageSize, Self::Error> {
        // This looks a little different than what we had before. Instead
        // of having a series of `if`s, we instead use a single `match` statement
        match v {
            // if 1, we have a special case, we can return the `Ok`
            // value with the maximum page size
            1 => Ok(PageSize(65_536u32)),
            // If we find 0 or 2-511, we found an invalid page size
            // we use the `format!` macro to include the provided value in the
            // error message
            0 | 2..=511 => Err(Error::InvalidPageSize(format!(
                "value must be >= 512, found: {}",
                v
            ))),
            // This will catch all values >= 512
            _ => {
                // Since we know it is large enough, we check if it is a power of 2
                if v.is_power_of_two() {
                    // success, we can cast the provided value to a `u32` and be done
                    Ok(PageSize(v as u32))
                } else {
                    // failed, return an error with an additional explanation
                    Err(Error::InvalidPageSize(format!(
                        "value must be a power of 2 found: {}",
                        v
                    )))
                }
            }
        }
    }
}

/// A value stored as Write Format Version or
/// Read Format Version
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum FormatVersion {
    /// Represents the rollback journal mode
    Legacy,
    /// Represents the Write Ahead Log mode
    WriteAheadLog,
    /// Represents any mode not 1 or 2, the value
    /// will be provided
    Unknown(u8),
}

impl From<u8> for FormatVersion {
    fn from(v: u8) -> Self {
        match v {
            1 => Self::Legacy,
            2 => Self::WriteAheadLog,
            _ => Self::Unknown(v),
        }
    }
}

/// Validate one of the payload/leaf fractions. If byte doesn't match
/// target will create an error with the provided name.
fn validate_fraction(byte: u8, target: u8, name: &str) -> Result<(), Error> {
    if byte != target {
        Err(Error::InvalidFraction(format!(
            "{} must be {}, found: {}", name, target, byte
        )))
    } else {
        Ok(())
    }
}

