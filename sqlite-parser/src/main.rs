// First we need to import our library like it is a dependency
// since it isn't officially part of our binary project. For right
// now we are going to just import all of the things individually
use sqlite_parser::{error::Error, header::parse_header};

fn main() -> Result<(), Error> {
    // still reading the whole file into a `Vec<u8>` and panicking if that fails
    let contents = std::fs::read("data.sqlite").expect("Failed to read data.sqlite");

    // we call our new `parse_header` function providing only the
    // first 100 bytes since that is the exact size of our header.
    // we "destructure" the tuple returned into 3 variables.
    let (page_size, write_format, read_format, _reserved_bytes, change_counter) =
        parse_header(&contents[0..100])?;
    println!(
        "page_size: {:?}, write_format: {:?}, read_format: {:?}, change_counter: {:?}",
        page_size, write_format, read_format, change_counter
    );

    Ok(())
}
