# sqlite-parser

Following up on the series [SQLite File Parser](https://freemasen.com//blog/sqlite-parser-pt-1/index.html).

## creating example database

```cmd
C:\home\bin\sqlite-tools-win32-x86-3390400\sqlite3
```

```
.open data.sqlite
```

```sql
CREATE TABLE user (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    name TEXT,
    email TEXT
);
INSERT INTO user (name, email)
VALUES ('Robert Masen', 'r@robertmasen.com'),
       ('Paul Bunyan', 'pb@example.com');
```

```sql
SELECT * FROM user;
```