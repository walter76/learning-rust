#[derive(Clone, Debug, PartialEq, Eq)]
struct Position {
    x: i32,
    y: i32,
}

trait Shape {
    fn pos(&self) -> Position;
}

struct Rectangle {
    position: Position,
}

impl Shape for Rectangle {
    fn pos(&self) -> Position {
        self.position.clone()
    }
}

struct Circle {
    position: Position,
}

impl Shape for Circle {
    fn pos(&self) -> Position {
        self.position.clone()
    }
}

struct Canvas<T: Shape> {
    shapes: Vec<T>,
}

impl<T: Shape> Canvas<T> {
    fn new() -> Self {
        Self {
            shapes: vec![],
        }
    }

    fn push(&mut self, shape: T) {
        self.shapes.push(shape);
    }
}

fn main() {
    let mut canvas = Canvas::new();

    canvas.push(Rectangle { position: Position {x: 20, y: 30 }});
    canvas.push(Circle { position: Position {x: 160, y: 25 }});
    canvas.push(Rectangle { position: Position {x: 100, y: 300 }});
    canvas.push(Rectangle { position: Position {x: 70, y: 30 }});
    canvas.push(Circle { position: Position {x: 360, y: 250 }});

    for shape in canvas.shapes.iter() {
        println!("{:?}", shape.pos());
    }
}
