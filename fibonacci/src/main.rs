use std::io;

fn fib(n: i32) -> i32 {
    if n == 0 {
        return 0;
    } else if n == 1 {
        return 1;
    }

    fib(n - 1) + fib(n - 2)
}

fn main() {
    println!("Generating Fibonacci Numbers");

    println!("Please enter n:");

    let mut n = String::new();

    io::stdin()
        .read_line(&mut n)
        .expect("Please enter a valid number for n");

    let n: i32 = n.trim().parse().expect("Please type a numbner!");

    println!("You entered {}", n);

    println!("Fibonacci: {}", fib(n))
}
