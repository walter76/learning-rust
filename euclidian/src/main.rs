use std::io;

fn gcd(counter: u32, denominator: u32) -> u32 {
    let mut u = counter;
    let mut v = denominator;

    while u > 0 {
        if u < v {
            let t = u;
            u = v;
            v = t;
        }

        u = u - v;
    }

    v
}

fn main() {
    println!("Plese input the counter.");

    let mut counter = String::new();
    io::stdin().read_line(&mut counter)
        .expect("Failed to read line");

    let counter: u32 = counter
        .trim()
        .parse()
        .expect("Cannot convert counter to int.");

    println!("Please input the denominator.");

    let mut denominator = String::new();
    io::stdin().read_line(&mut denominator)
        .expect("Failed to read line");

    let denominator: u32 = denominator
        .trim()
        .parse()
        .expect("Cannot convert denominator to int.");

    println!("Fraction is {}/{}.", counter, denominator);
    println!("gcd is {}", gcd(counter, denominator));
}
