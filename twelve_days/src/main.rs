fn main() {
    let first_sentence_p1 = "On the";
    let first_sentence_p2 = "day of Christmas, my true love gave to me";
    let day_numbers = [
        "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth",
        "tenth", "eleventh", "twelfth",
    ];
    let presents = [
        "a partridge in a pear tree",
        "two turtle doves",
        "three French hens",
        "four calling birds",
        "five golden rings",
        "six geese a-layin'",
        "seven swans a-swimmin'",
        "eight maids a-milkin'",
        "nine lords a-leapin'",
        "ten ladies dancin'",
        "eleven pipers pipin'",
        "twelve drummers drummin'",
    ];

    let mut number_of_presents = 1;
    for day in day_numbers.iter() {
        println!("{} {} {}", first_sentence_p1, day, first_sentence_p2);

        let mut index = number_of_presents;
        while index > 0 {
            print!("{}", presents[index - 1]);

            if index > 2 {
                print!(", ")
            } else if index > 1 {
                print!(" and ")
            }

            if index % 2 == 0 {
                println!()
            }

            index = index - 1;
        }
        println!();

        number_of_presents = number_of_presents + 1;
        println!();
    }
}
