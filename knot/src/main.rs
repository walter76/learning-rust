use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::TextureQuery;
use sdl2::render::{Canvas, Texture};
use sdl2::video::Window;
use std::time::Duration;

struct App {
    canvas: Canvas<Window>,
    event_pump: sdl2::EventPump,
}

impl App {
    pub fn new() -> Result<Self, String> {
        let sdl_context = sdl2::init()?;
        let video_subsystem = sdl_context.video()?;

        let window = video_subsystem
            .window("knot", 800, 600)
            .position_centered()
            .build()
            .unwrap();

        let canvas = window.into_canvas().accelerated().build().unwrap();

        let event_pump = sdl_context.event_pump()?;

        Ok(Self { canvas, event_pump })
    }

    pub fn canvas_mut(&mut self) -> &mut Canvas<Window> {
        &mut self.canvas
    }

    pub fn event_pump_mut(&mut self) -> &mut sdl2::EventPump {
        &mut self.event_pump
    }
}

const FONT_PATH: &str = r"Roboto-Regular.ttf";
const FONT_POINT_SIZE: u16 = 14;

struct FontLoader {
    ttf_context: sdl2::ttf::Sdl2TtfContext,
}

impl FontLoader {
    pub fn new() -> Self {
        let ttf_context = sdl2::ttf::init().unwrap();

        Self { ttf_context }
    }

    pub fn load_font<'ttf>(&'ttf self) -> sdl2::ttf::Font<'ttf, 'static> {
        self.ttf_context
            .load_font(FONT_PATH, FONT_POINT_SIZE)
            .unwrap()
    }
}

fn main() -> Result<(), String> {
    let mut app = App::new()?;

    let font_loader = FontLoader::new();
    let texture_creator = app.canvas_mut().texture_creator();
    let texture = render_text(&font_loader, &texture_creator);

    'running: loop {
        prepare_scene(&mut app, &texture)?;

        if !do_input(&mut app) {
            break 'running;
        }

        present_scene(&mut app);

        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }

    Ok(())
}

fn do_input(app: &mut App) -> bool {
    for event in app.event_pump_mut().poll_iter() {
        match event {
            Event::Quit { .. }
            | Event::KeyDown {
                keycode: Some(Keycode::Escape),
                ..
            } => {
                return false;
            }
            _ => {}
        }
    }

    true
}

fn prepare_scene(app: &mut App, texture: &Texture) -> Result<(), String> {
    draw(app.canvas_mut(), texture)
}

fn draw(canvas: &mut Canvas<Window>, texture: &Texture) -> Result<(), String> {
    canvas.set_draw_color(Color::WHITE);
    canvas.clear();

    canvas.set_draw_color(Color::BLACK);
    canvas.draw_rect(Rect::new(0, 0, 80, 20))?;

    blit(canvas, texture, 0, 0).unwrap();

    Ok(())
}

fn blit(canvas: &mut Canvas<Window>, texture: &Texture, x: i32, y: i32) -> Result<(), String> {
    let TextureQuery { width, height, .. } = texture.query();
    canvas.copy(texture, None, Some(Rect::new(x, y, width, height)))
}

fn present_scene(app: &mut App) {
    app.canvas_mut().present();
}

fn render_text<'a>(
    font_loader: &FontLoader,
    texture_creator: &'a sdl2::render::TextureCreator<sdl2::video::WindowContext>,
) -> Texture<'a> {
    let font = font_loader.load_font();
    let surface = font.render("Test").solid(Color::BLACK).unwrap();

    texture_creator
        .create_texture_from_surface(&surface)
        .unwrap()
}
